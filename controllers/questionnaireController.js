/**
 * Created by var_dump on 1/5/2018.
 */

exports.dataPrepare = function (db, groupId, userId, withAnswers) {

    return new Promise((resolve, reject) => {

        db.select('*').where({'id': groupId}).get('question_groups', function (err, rows, fields) {
            if(rows.length === 0){
                reject(false);
                return;
            }
            var title = rows[0]['title'];
            var description = rows[0]['description'];

            db.select('questions.id as qid, questions.group_id,questions.question,questions.type,' +
                'question_answers.id as qaid, question_answers.label')
                .join('question_answers', 'question_answers.question_id = questions.id', 'left')
                .where({
                    'questions.group_id': groupId,
                });

            if (withAnswers) {
                db.select('user_answers.question_answer_id,user_answers.text_answer')
                db.join('user_answers', 'user_answers.question_id = questions.id', 'left');
                db.where({
                    'user_answers.user_id': userId
                });
            }

            db.order_by('questions.order asc')
                .get('questions', function (err, rows, fields) {
                    // console.log(err, rows);
                    var dataMap = new Map();
                    for (var i = 0; i < rows.length; i++) {
                        if (typeof dataMap.get(rows[i]['qid']) === 'undefined') {
                            var obj = {
                                question_id: rows[i]['qid'],
                                question: rows[i]['question'],
                                type: rows[i]['type'],
                                options: [],
                                answers: [],
                            };
                            if (rows[i]['label']) {
                                obj['options'][rows[i]['qaid']] = rows[i]['label'];
                            }
                            if (withAnswers) {
                                if (rows[i]['question_answer_id']) {
                                    if (!obj['answers'].includes(rows[i]['question_answer_id'])) {
                                        obj['answers'].push(rows[i]['question_answer_id']);
                                    }
                                } else if (rows[i]['text_answer']) {
                                    obj['answers'].push(rows[i]['text_answer']);
                                }
                            }

                            dataMap.set(rows[i]['qid'], obj);
                        } else {
                            var obj = dataMap.get(rows[i]['qid']);
                            if (rows[i]['label']) {
                                obj['options'][rows[i]['qaid']] = rows[i]['label'];
                            }

                            if (withAnswers) {
                                if (rows[i]['question_answer_id']) {
                                    if (!obj['answers'].includes(rows[i]['question_answer_id'])) {
                                        obj['answers'].push(rows[i]['question_answer_id']);
                                    }
                                } else if (rows[i]['text_answer']) {
                                    obj['answers'].push(rows[i]['text_answer']);
                                }
                            }

                            dataMap.set(rows[i]['qid'], obj);
                        }
                    }
                    // console.log(dataMap);
                    resolve({
                        title: title,
                        description: description,
                        groupId: groupId,
                        data: dataMap
                    });
                });
        });
    })
        ;

};

exports.isQuestionnaireFinished = function (db, groupId, userID) {
    return new Promise((resolve, reject) => {
        db.select('*').where({user_id: userID, group_id: groupId}).get('user_groups', (err, rows, fields) => {
                if (!err){
                    resolve (rows.length > 0  ? true: false);

                }else {
                    reject (err);
                }
        });
    })


}