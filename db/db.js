/**
 * Created by var_dump on 12/31/2017.
 */
var Db = require('mysql-activerecord');
var db = new Db.Adapter({
    server: 'localhost',
    username: 'root',
    password: '',
    database: 'mopq',
    reconnectTimeout: 2000
});

module.exports = db;