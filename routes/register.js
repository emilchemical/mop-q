var express = require('express');
var router = express.Router();
var bcrypt = require('bcrypt-nodejs');

router.get('/', function (req, res, next) {
    res.locals.urlState = 'register';
    res.render('register');
    next();
});

router.post('/', function (req, res, next) {
    console.log(req.body);
    if (req.body.email && req.body.password && req.body.fullName) {
        var db = req.app.db;
        // var ind = false;
        db.select('*').where({email: req.body.email});
        db.get('users', function (err, rows, fields) {
            if (!err) {
                if (rows.length !== 0) {
                    res.render('register', {message: 'User ' + req.body.email + ' je već registrovan!'});
                    return;
                }
                else {
                    db.insert('users', {
                        full_name: req.body.fullName,
                        email: req.body.email,
                        password: bcrypt.hashSync(req.body.password),
                        user_type: 'user'
                    }, function (err, info) {
                        console.log(err, info);
                        res.redirect('/');
                    });
                }
            } else {
                res.render('register', {message: 'Greška sa konekcijom ka bazi podataka!!'});
            }
        });

    }
    else {
        res.render('register', {message: 'Svi podaci moraju biti unijeti!'});
    }
});

module.exports = router;
