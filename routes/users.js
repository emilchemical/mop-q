var express = require('express');
var router = express.Router();

/**
 * Izlistavanje dostupnih anketa
 */
router.get('/', function (req, res, next) {
    res.locals.urlState = 'lista_anketa';
    var db = req.app.db;
    var userId = req.session.userId;
    var itemsPerPage = 10;
    var page = req.query.page;
    var offeset = (page) ? page : 0;
    db
        .select('SQL_CALC_FOUND_ROWS question_groups.*,user_groups.id as gid,user_groups.user_id as userId')
        .limit(itemsPerPage, offeset)
        .where({
            is_active: 1
        })
        .join('user_groups', 'user_groups.group_id = question_groups.id and user_groups.user_id = '+userId, 'left')
        .get('question_groups', function (err, rows, fields) {
            console.log(err, rows);

            db.query('SELECT FOUND_ROWS() as total', function (err, results) {
                var data = {
                    total: results[0].total,
                    rows: rows
                };
                console.log(data);
                res.render('dostupne_ankete', data);
            });
        });
});

/**
 * Izlistavanje pitanja
 */
router.get('/answers/:groupId', function (req, res, next) {
    var db = req.app.db;
    var groupId = req.params.groupId;
    var userId = req.session.userId;

    var questionnaireService = require('../controllers/questionnaireController');

    questionnaireService.dataPrepare(db, groupId, userId, true).then((data) => {
        console.log(data, groupId, userId);
        res.render('question-answers', data)
    }).catch(reaseon =>{
        res.redirect('/');
    });
});

/**
 * Prikazi formu za popunjavanje ankete,
 * ukoliko je anketa vecpopunjena redirektuj na home page
 */
router.get('/questionnaire/:groupId', function (req, res, next) {
    var db = req.app.db;
    var groupId = req.params.groupId;
    var userId = req.session.userId;

    res.locals.urlState = 'register';

    var questionnaireService = require('../controllers/questionnaireController');
    questionnaireService.isQuestionnaireFinished(db, groupId, userId).then(status => {
        if (status) {
            res.redirect('/users');
        } else {
            questionnaireService.dataPrepare(db, groupId, userId, false).then((data) => {
                console.log(data, groupId, userId);
                res.render('questionnaire', data)
            }).catch(reaseon =>{
                console.log('reasonnnnn',reaseon);
                res.redirect('/');
            });
        }
    });

});

/**
 * Cuvanje odgovora korisnika
 */
router.post('/questionnaire/:groupId', function (req, res, next) {
    // console.log(req.body);
    // console.log(req.session);

    var db = req.app.db;
    var groupId = req.params.groupId;
    var userId = req.session.userId;

    var questionnaireService = require('../controllers/questionnaireController');
    questionnaireService.isQuestionnaireFinished(db, groupId, userId).then(status => {
        if (status) {
            res.redirect('/users');
        } else {
            if (typeof userId !== 'undefined') {
                var formData = req.body;
                console.log(formData);
                for (var property in formData) {
                    if (Object.prototype.hasOwnProperty.call(formData, property)) {
                        // if (formData.hasOwnProperty(property)) {
                        var data = property.split('_');
                        if (data.length !== 2) {
                        } else {
                            var questionId = data[1];
                            if (data[0] === 'radio') {
                                db.insert('user_answers', {
                                    question_id: questionId,
                                    question_answer_id: req.body[property],
                                    user_id: userId
                                }, function (err, info) {
                                    console.log(err, info);
                                });
                            } else if (data[0] === 'checkbox') {
                                req.body[property].forEach(function (qaid) {
                                    db.insert('user_answers', {
                                        question_id: questionId,
                                        question_answer_id: qaid,
                                        user_id: userId
                                    }, function (err, info) {
                                        console.log(err, info);
                                    });
                                });
                            } else if (data[0] === 'text') {
                                db.insert('user_answers', {
                                    question_id: questionId,
                                    text_answer: req.body[property],
                                    user_id: userId
                                }, function (err, info) {
                                    console.log(err, info);
                                });
                            }
                        }
                    }
                }
                db.insert('user_groups', {user_id: userId, group_id: groupId}, function (err, info) {
                    console.log(err, info);
                    res.render('success_page');
                });
            }
            else {
                console.log('niste ulogovani');
                res.redirect('/');
            }
        }
    });

});

module.exports = router;
