var express = require('express');
var router = express.Router();

/**
 * Renderuje html formu za kreiranje anketa (question_groups)
 */
router.get('/create', function (req, res, next) {
    res.render('create');
});

/**
 * Handler post requesta
 */
router.post('/create', function (req, res, next) {
    var db = req.app.db;
    db.insert('question_groups', {title: req.body.naziv, description: req.body.opis}, function (err, info) {
        console.log(err, info);
        res.redirect('/admin');
    });
});

/**
 * Forma za kreiranje pitanja unutar ankete i izlistavanje postojecih pitanja
 */
router.get('/questions/:groupId', function (req, res, next) {
    var db = req.app.db;
    var groupId = req.params.groupId;

    db.select('questions.id as qid, questions.group_id,questions.question,questions.type,' +
        'question_answers.id as qaid, question_answers.label')
        .join('question_answers', 'question_answers.question_id = questions.id', 'left')
        .where({
            'questions.group_id': groupId
        })
        .order_by('questions.order asc')
        .get('questions', function (err, rows, fields) {
            // console.log(err, rows);
            var dataMap = new Map();
            for (var i = 0; i < rows.length; i++) {
                if (typeof dataMap.get(rows[i]['qid']) === 'undefined') {
                    var obj = {
                        question_id: rows[i]['qid'],
                        question: rows[i]['question'],
                        answers: [rows[i]['label']],
                        type: [rows[i]['type']]
                    };
                    if (rows[i]['label']) {
                        obj['answers'] = [rows[i]['label']];
                    } else {
                        obj['answers'] = [];
                    }
                    dataMap.set(rows[i]['qid'], obj);
                } else {
                    var obj = dataMap.get(rows[i]['qid']);
                    if (rows[i]['label']) {
                        obj['answers'].push(rows[i]['label']);
                    }
                    dataMap.set(rows[i]['qid'], obj);
                }
            }
            console.log(dataMap);
            res.render('questions', {groupId: groupId, data: dataMap});
        });
});

/**
 * Handler za kreiranje novog pitanja
 * Puni tabele questions (definiciej pitanja u anketi )i question_answers - (ponudjeni ogovori za jedno pitanje) ukoliko je potrebno
 */
router.post('/questions/:groupId', function (req, res, next) {
    var db = req.app.db;
    var groupId = req.params.groupId;
    var question = req.body.question;
    var type = req.body.question_type;
    if (typeof req.body.questions !== 'undefined') {
        var questions = req.body.questions.split(',');
    } else {
        var questions = [];
    }

    if (type === 'yes-no') {
        type = 'single';
        questions = ['Da', 'Ne'];
    }

    // console.log(req.body, groupId, questions);
    db.insert('questions', {group_id: groupId, question: question, type: type}, function (err, info) {
        console.log(err, info);
        var data = [];
        for (var i = 0; i < questions.length; i++) {
            const row = {
                question_id: info.insertId,
                label: questions[i],
            };
            data.push(row);
        }
        if (data.length > 0) {
            db.insert('question_answers', data, function (err, info) {
                console.log(err, info);
                res.redirect('/admin/questions/' + groupId);
            });
        } else {
            res.redirect('/admin/questions/' + groupId);
        }
    });
});

/**
 * Brisanje  pitanja unutar ankete po id-u
 */
router.get('/questions/:groupId/:questionId', function (req, res, next) {
    var questionId = req.params.questionId;
    var groupId = req.params.groupId;
    var db = req.app.db;
    db
        .where({id: questionId})
        .delete('questions', function (err) {
            db.where({question_id: questionId}).delete('question_answers', function (err) {
                if (!err) {
                    console.log('Deleted Answers!')
                }
            });
            if (!err) {
                console.log('Deleted Questions!')
            }
            res.redirect('/admin/questions/' + groupId);
        });

});

/**
 * Izlistavanje anketa
 */
router.get('/', function (req, res, next) {
    res.locals.urlState = 'lista_anketa';
    var db = req.app.db;
    var itemsPerPage = 2;
    var page = typeof (req.query.page) !== 'undefined'? parseInt(req.query.page) : 1;

    var offeset = (page) ? (page-1)*itemsPerPage : 0;
    db.select('SQL_CALC_FOUND_ROWS *').limit(itemsPerPage, offeset).get('question_groups', function (err, rows, fields) {
        db.query('SELECT FOUND_ROWS() as total', function (err, results) {
            var data = {
                total: results[0].total,
                rows: rows,
                page: page
            };
            console.log(data);
            res.render('admin', data);

        });

    });

});

/**
 * Deaktivacija
 */
router.get('/deactivate/:groupId', function (req, res, next) {
    var db = req.app.db;
    var groupId = req.params.groupId;
    db.where({id: groupId}).update('question_groups', {is_active: 0}, function (err) {
        res.redirect('/');
        next();
    });
});

/**
 * Aktivacija
 */
router.get('/activate/:groupId', function (req, res, next) {
    var db = req.app.db;
    var groupId = req.params.groupId;
    db.where({id: groupId}).update('question_groups', {is_active: 1}, function (err) {
        res.redirect('/');
        next();
    });
});

/**
 * Brisanje definicije ankete
 */
router.get('/delete/:groupId', function (req, res, next) {
    var db = req.app.db;
    var groupId = req.params.groupId;
    db.where({id: groupId}).delete('question_groups', function (err) {
        res.redirect('/');
        next();
    });
});

module.exports = router;
