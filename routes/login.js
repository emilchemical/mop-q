var express = require('express');
var router = express.Router();
var bcrypt = require('bcrypt-nodejs');

/**
 * Rendering login page
 */
router.get('/', function (req, res, next) {
    res.locals.urlState = 'login';
    res.render('index', {title: 'MOP-Q'});
});

/**
 * Login page post request hanndler
 */
router.post('/', function (req, res, next) {
    // console.log(req.body);
    if (req.body.email && req.body.password) {
        var db = req.app.db;
        db.select('*').where({email: req.body.email});
        db.get('users', function (err, rows, fields) {
            // console.log(rows);
            if (!err) {
                if (rows.length === 0) {
                    res.render('index', {message: 'User nije registrovan!'});
                    return;
                }
                else {
                    if (bcrypt.compareSync(req.body.password, rows[0].password)) {
                        req.session.username = req.body.email;
                        req.session.type = rows[0].user_type;
                        req.session.userId = rows[0].id;
                        req.session.fullName = rows[0].full_name;
                        if (rows[0].user_type === 'admin') {
                            res.redirect('/users/admin');
                            next();
                        } else if (rows[0].user_type === 'user') {
                            res.redirect('/users');
                        }
                        // res.render('index', {message: 'sve ok'});
                    } else {
                        res.render('index', {message: 'Lozinka nije dobra!'});
                    }
                    console.log(rows);
                }
            } else {
                res.render('index', {message: 'Greška sa konekcijom ka bazi podataka!!'});
            }
        });

    } else {
        res.render('index', {message: 'Korisnicko ime i lozinka moraju biti unijeti!'});
    }
});

module.exports = router;
